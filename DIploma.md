# Дипломный практикум в Yandex.Cloud
  * [Цели:](#цели)
  * [Этапы выполнения:](#этапы-выполнения)
     * [Создание облачной инфраструктуры](#создание-облачной-инфраструктуры)
     * [Создание Kubernetes кластера](#создание-kubernetes-кластера)
     * [Создание тестового приложения](#создание-тестового-приложения)
     * [Подготовка cистемы мониторинга](#подготовка-cистемы-мониторинга)
     * [Установка и настройка CI/CD](#установка-и-настройка-cicd)
     * [Установка и настройка Atlantis](#установка-и-настройка-atlantis)

---
## Цели:

1. Подготовить облачную инфраструктуру на базе облачного провайдера Яндекс.Облако.
2. Запустить и сконфигурировать Kubernetes кластер.
3. Установить и настроить систему мониторинга.
4. Настроить и автоматизировать сборку тестового приложения с использованием Docker-контейнеров.
5. Настроить CI для автоматической сборки.
6. Настроить CD для автоматического развёртывания приложения.

---

## Этапы выполнения:

### Рабочая среда

  - Машина Windows 11 23H2, powershell 7.4.1, terraform 1.5.5, Yandex Cloud CLI 0.115.0
  - Машина Ubuntu 22.04.3, helm 3.14.2, terraform 1.5.5, kubectl 1.28.2, Yandex Cloud CLI 0.115.0

### Создание облачной инфраструктуры

1. Установил версию Terraform 1.5.5

    ![terraform_version](./img/terraform_version.png)

2. Создал сервисный аккаунт для управления инфраструктурой (пришлось дать admin что бы была возможность назначать роли другим сервисным аккаунтам)

    ```powershell
    yc iam service-account create --name filipp0vap-diploma-netology
    yc iam service-account list
    yc resource-manager folder add-access-binding *********lehtk1brjos --role admin --subject serviceAccount:*********m6ei7pf22dk
    yc iam key create --service-account-id *********m6ei7pf22dk --folder-name netology --output key.json
    yc config profile create filipp0vap-diploma-netology


    yc config set service-account-key key.json
    yc config set cloud-id *********8s5jhr4m42o
    yc config set folder-id *********lehtk1brjos

    $Env:YC_TOKEN=$(yc iam create-token)
    $Env:YC_CLOUD_ID=$(yc config get cloud-id)
    $Env:YC_FOLDER_ID=$(yc config get folder-id)
    ```

3. Создал бакет для хранения состояния инфраструктуры создаваемой Terraform
    Написал манифесты для создания [bucket](./terraform/bucket/bucket.tf) и [сервисного аккаунта](./terraform/bucket/service_account.tf) и ключей для него

    ![terraform_bucket_01](./img/terraform_bucket_01.png)

    ![terraform_bucket_02](./img/terraform_bucket_02.png)

    ![terraform_bucket_03](./img/terraform_bucket_03.png)

4. Ключи записываем в переменные

    ```powershell
    $Env:AWS_ACCESS_KEY_ID="YCAJEru*********R27JPcqP"
    $Env:AWS_SECRET_ACCESS_KEY="YCNkqAUMlL8A**********P0B8UlwOWGL1qmz"
    ```

---
### Создание Kubernetes кластера

Я решил воспользоваться сервисом [Yandex Managed Service for Kubernetes](https://cloud.yandex.ru/services/managed-kubernetes)

1. Написал следующие манифесты:

    - [Сети](./terraform/network.tf)
    - [Сервис аккаунты и ключи](./terraform/service_account.tf)
    - [K8s кластер, группы нод и докер реестр](./terraform/main.tf)
    - В [Provider](/terraform/provider.tf) прописал созданный ранее бакет для хранения состояния

2. Создаем кластер

    ![terraform_cluster_01](./img/terraform_cluster_01.png)

    ![terraform_cluster_02](./img/terraform_cluster_02.png)

    ![terraform_cluster_03](./img/terraform_cluster_03.png)

    ![terraform_cluster_04](./img/terraform_cluster_04.png)

3. Проверяем результат в консоли

    ![terraform_cluster_05](./img/terraform_cluster_05.png)


4. Подключаемся к кластеру и проверяем результат

    ![terraform_cluster_06](./img/terraform_cluster_06.png)

    ![terraform_cluster_07](./img/terraform_cluster_07.png)


5. Так же проверяем что в бакете сохранилось состояние terraform

    ![terraform_cluster_08](./img/terraform_cluster_08.png)

---

### Создание тестового приложения

1. Создал [репозиторий](https://gitlab.com/netology1004609/devops-diplom-app) с тестовым приложением

2. Сделал простенький [Dockerfile](https://gitlab.com/netology1004609/devops-diplom-app/-/blob/main/Dockerfile)

    ```Dockerfile
    FROM nginx:1.25.4

    COPY html /usr/share/nginx/html/
    ```

3. А так же [манифест](https://gitlab.com/netology1004609/devops-diplom-app/-/blob/main/diploma-manifest.yaml.tmp) k8s для деплоя

    ```yaml
    ---
    apiVersion: apps/v1
    kind: Deployment
    metadata:
      name: diploma-app-deployment
      namespace: diploma-app
    spec:
      replicas: 2
      strategy:
        type: RollingUpdate
        rollingUpdate:
          maxSurge: 50%
          maxUnavailable: 50%
      selector:
        matchLabels:
          app: diploma-app-deployment
      template:
        metadata:
          labels:
            app: diploma-app-deployment
        spec:
          containers:
          - name: diploma-app
            image: cr.yandex/$REGISTRY_ID/devops-diploma-app:$DOCKER_IMAGE_TAG
            resources:
              limits:
                memory: 512Mi
                cpu: "0.5"
              requests:
                memory: 256Mi
                cpu: "0.25"
            ports:
            - containerPort: 80
    ...
    ---
    apiVersion: v1
    kind: Service
    metadata:
      name: diploma-app-svc
      namespace: diploma-app
    spec:
      type: LoadBalancer
      selector:
        app: diploma-app-deployment
      ports:
        - protocol: TCP
          port: 80
          targetPort: 80

    ...

    ```

4. Создал неймспейс diploma-app в кластере

    ```bash
    kubectl create ns diploma-app
    ```
    ![create_app_01](./img/create_app_01.png)

---

### Подготовка cистемы мониторинга

1. Я воспользовался пакетом [kube-prometheus](https://github.com/prometheus-operator/kube-prometheus). 
Так как K8s кластер у меня версии 1.26, я взял ветку [release-0.13](https://github.com/prometheus-operator/kube-prometheus/tree/release-0.13) и скопировал манифесты себе в папку [monitoring](./monitoring/)

2. Так же я написал [манифест](./monitoring/grafana-service.yaml) с сервисом типа LoadBalancer для доступа к Grafana извне

3. Применяем манифесты

    ```bash
    kubectl apply --server-side -f manifests/setup
    kubectl wait --for condition=Established --all CustomResourceDefinition --namespace=monitoring
    kubectl apply -f manifests/
    kubectl apply -f grafana-service.yaml
    ```

    ![monitoring_01](./img/monitoring_01.png)

    ![monitoring_02](./img/monitoring_02.png)

4. Открываем веб интерфейс и проверяем что дашборды работают

    ![monitoring_03](./img/monitoring_03.png)

    ![monitoring_04](./img/monitoring_04.png)

---
### Установка и настройка CI/CD

Для реализации CI\CD был выбран [gitlab.com](https://gitlab.com)

1. В первую очередь создаем gitlab runner в ранее созданном [репозитории](https://gitlab.com/netology1004609/devops-diplom-app)

    ![gitlab_ci_01](./img/gitlab_ci_01.png)

2. Копируем токен раннера

    ![gitlab_ci_01](./img/gitlab_ci_02.png)

3. В нашем кластере создаем неймспейс и [секрет](./k8s/gitlab-runner-secret.yaml) с токеном

    ```bash
    cd k8s/
    kubectl create ns gitlab-runner
    sed -i 's/REDACTED/'"$(echo "glrt-zYfz******L2PR2oznW2q" | base64)"'/g' gitlab-runner-secret.yaml
    kubectl apply --namespace gitlab-runner -f gitlab-runner-secret.yaml
    ```

4. Далее создаем configmap для доступа к docker registry из раннера.
    ```bash
    cd ../terraform/

    yc iam key create --service-account-name k8s-node-sa -o docker_registry_key.json
    
    cat docker_registry_key.json | docker login \
      --username json_key \
      --password-stdin \
      cr.yandex

    kubectl create configmap docker-client-config --namespace gitlab-runner --from-file ~/.docker/config.json
    ```

    ![gitlab_ci_03](./img/gitlab_ci_03.png)

5. Следующим шагом применяем helm chart с поднятием раннеров у нас в кластере

    ```bash
    cd ../helm
    helm repo add gitlab https://charts.gitlab.io
    helm repo update gitlab
    helm install --namespace gitlab-runner gitlab-runner -f ./values.yaml gitlab/gitlab-runner
    ```

    ![gitlab_ci_04](./img/gitlab_ci_04.png)

6. Открываем настройки CI\CD в репозитории и проверяем что раннер подключился

    ![gitlab_ci_05](./img/gitlab_ci_05.png)

7. Раннер готов, теперь собираем и пушим в реестр наш образ для деплоя в k8s

    ```bash
    cd ../k8s
    docker build -t cr.yandex/crp8itk07jnp5jfa7jgl/kubectl:1.29.2-debian-12-r3-gettext .
    docker push cr.yandex/crp8itk07jnp5jfa7jgl/kubectl:1.29.2-debian-12-r3-gettext
    ```

    ![gitlab_ci_06.png](./img/gitlab_ci_06.png)

8. Теперь подключаем Gitlab к нашему k8s кластеру

    ![gitlab_ci_07.png](./img/gitlab_ci_07.png)

9. Создаем токен и выполняем предложенные команды

    ![gitlab_ci_08.png](./img/gitlab_ci_08.png)

    ![gitlab_ci_09.png](./img/gitlab_ci_09.png)

10. Создаем в репозитории переменную REGISTRY_ID

    ![gitlab_ci_10.png](./img/gitlab_ci_10.png)

11. Cоздаем файл [.gitlab-ci.yml](https://gitlab.com/netology1004609/devops-diplom-app/-/blob/main/.gitlab-ci.yml) со следующим содержимым

    ```yaml
    stages:
      - build
      - deploy

    default:
      image: docker:25.0.3
      tags: 
        - docker
      services:
        - name: docker:25.0.3-dind
          command: ["--tls=false"]
          alias: thedockerhost

    variables:
      DOCKER_DRIVER: overlay2
      DOCKER_HOST: tcp://thedockerhost:2375
      DOCKER_TLS_CERTDIR: ""
      DOCKER_IMAGE_TAG: $CI_JOB_ID

    build:
      before_script:
        - apk add --update --no-cache git
      rules:
        - if: '$CI_COMMIT_TAG'
          variables:
            DOCKER_IMAGE_TAG: $CI_COMMIT_TAG
        - if: $CI_PIPELINE_SOURCE == "push"
      stage: build
      script:
        - docker build -t cr.yandex/$REGISTRY_ID/devops-diploma-app:$DOCKER_IMAGE_TAG .
        - docker push cr.yandex/$REGISTRY_ID/devops-diploma-app:$DOCKER_IMAGE_TAG

    deploy:
      rules:
        - if: '$CI_COMMIT_TAG'
          variables:
            DOCKER_IMAGE_TAG: $CI_COMMIT_TAG
      image: 
        name: cr.yandex/$REGISTRY_ID/kubectl:1.29.2-debian-12-r3-gettext
      stage: deploy
      script:
        - kubectl config get-contexts
        - kubectl config use-context netology1004609/devops-diplom-app:filipp0vap-diploma
        - envsubst < diploma-manifest.yaml.tmp > diploma-manifest.yaml
        - kubectl apply -f diploma-manifest.yaml
    ```

12. Пушим какой нибудь коммит в гит

    ![gitlab_ci_11.png](./img/gitlab_ci_11.png)

13. Проверяем что запустился билд и ждем пока он завершиться пушем образа в наш реестр

    ![gitlab_ci_12.png](./img/gitlab_ci_12.png)

    ![gitlab_ci_13.png](./img/gitlab_ci_13.png)

14. Проверяем что наш образ появился в реестре

    ![gitlab_ci_14.png](./img/gitlab_ci_14.png)

15. Создадим тег и проверим что теперь образ будет с тегом из гита, а так же произойдет деплой

    ![gitlab_ci_15.png](./img/gitlab_ci_15.png)
    
    ![gitlab_ci_16.png](./img/gitlab_ci_16.png)

    ![gitlab_ci_17.png](./img/gitlab_ci_17.png)
    
    ![gitlab_ci_18.png](./img/gitlab_ci_18.png)

16. Проверяем что образ в реестре с нужным нам тегом

    ![gitlab_ci_19.png](./img/gitlab_ci_19.png)

17. Проверяем что под создался в кластере

    ![gitlab_ci_20.png](./img/gitlab_ci_20.png)

18. Проверяем работу приложения по [ссылке](http://158.160.145.168/)

    ![gitlab_ci_21.png](./img/gitlab_ci_21.png)


19. Profit! 🎉🥳🎊

---
### Установка и настройка Atlantis

1. Деплоим атлантис

    ```bash
    kubectl create secret generic atlantis-terraformrc --from-file=./.terraformrc
    helm repo add runatlantis https://runatlantis.github.io/helm-charts
    cd helm/atlantis
    helm inspect values runatlantis/atlantis > values.yaml
    ```
2. Настраиваем [манифест](./helm/atlantis/values.yaml) под себя

3. Применяем чарт
    
    ```bash
    helm install atlantis runatlantis/atlantis -f values.yaml
    ```
    ![atlantis_01](./img/atlantis_01.png)

4. Добавляем настройки atlantis в репу - [atlantis.yaml](./atlantis.yaml)

5. Добавляем webhook

    ![atlantis_02](./img/atlantis_02.png)

6. Создаем МР из новой ветки и видим что создался план
 
    ![atlantis_03](./img/atlantis_03.png)

    ![atlantis_04](./img/atlantis_04.png)

7. Применяем

    ![atlantis_05](./img/atlantis_05.png)

    Если открыть интерфейс atlantis, то можно увидеть процесс выполнения

    ![atlantis_06](./img/atlantis_06.png)

    Так же можно увидеть в консоли облака что идет применение изменений

    ![atlantis_07](./img/atlantis_07.png)

8. Изменения успешно применены

    ![atlantis_08](./img/atlantis_08.png)

    ![atlantis_09](./img/atlantis_09.png)

